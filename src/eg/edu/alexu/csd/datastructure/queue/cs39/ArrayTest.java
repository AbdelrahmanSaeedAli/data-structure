package eg.edu.alexu.csd.datastructure.queue.cs39;

import static org.junit.Assert.*;

public class ArrayTest {
	ArrayBasedQueue queue = new ArrayBasedQueue(10);

	public void test() {

		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.enqueue(6);
		assertEquals(queue.size(), 6);
		assertEquals(queue.isEmpty(), false);
		assertEquals(queue.dequeue(), 1);
		assertEquals(queue.dequeue(), 2);
		assertEquals(queue.dequeue(), 3);
		assertEquals(queue.dequeue(), 4);
		assertEquals(queue.dequeue(), 5);
		assertEquals(queue.size(), 1);
		queue.dequeue();
		assertEquals(queue.size(), 0);
		assertEquals(queue.isEmpty(), true);
		assertEquals(queue.dequeue(), null);

	}

}
