package eg.edu.alexu.csd.datastructure.queue.cs39;

import eg.edu.alexu.csd.datastructure.queue.IQueue;
import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;

public class LinkedBasedQueue implements IQueue, ILinkedBased {
	private Node head = new Node();
	private Node tail = new Node();
	private int size = 0;

	public void enqueue(Object item) {
		Node node = new Node();
		node.setElement(item);
		node.setNext(null);
		if (size == 0) {
			head = node;
		} else
			tail.setNext(node);
		tail = node;
		size++;
	}

	public Object dequeue() {
		if (size == 0)
			throw null;
		Object temp = head.getElement();
		head = head.getNext();
		size--;
		if (size == 0)
			tail = null;
		return temp;
	}

	public boolean isEmpty() {
		return (size < 1);
	}

	public int size() {
		return size;
	}
}