package eg.edu.alexu.csd.datastructure.queue.cs39;

import eg.edu.alexu.csd.datastructure.queue.IQueue;
import eg.edu.alexu.csd.datastructure.queue.IArrayBased;

public class ArrayBasedQueue implements IQueue, IArrayBased {
	private int r = 0;
	private int n;
	private Object[] Q;

	public ArrayBasedQueue(int n) {
		this.n = n;
		this.Q = new Object[n];
	}

	public void enqueue(Object item) {
		if (r == n) {
			throw null;
		}
		Q[r] = item;
		r++;
	}

	public Object dequeue() {
		if (r == 0) {
			throw null;
		}
		Object temp = Q[0];

		for (int i = 0; i < r - 1; i++) {
			Q[i] = Q[i + 1];
		}
		r = r - 1;
		return temp;
	}

	public boolean isEmpty() {
		return r < 1;
	}

	public int size() {
		return r;
	}
}