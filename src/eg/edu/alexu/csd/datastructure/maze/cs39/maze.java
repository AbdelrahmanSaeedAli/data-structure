package eg.edu.alexu.csd.datastructure.maze.cs39;

import java.io.FileNotFoundException;
import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;
import eg.edu.alexu.csd.datastructure.queue.cs39.LinkedBasedQueue;
import eg.edu.alexu.csd.datastructure.stack.cs39.MyStack;

public class maze implements IMazeSolver {
	char[][] map;
	tile T;
	boolean foundEnd;
	int sizePath;
	int[][] path = new int[100][2];
	boolean noEntry = true;
	boolean noExit = true;

	public int[][] compress(int[][] arr1, int size) {
		final int[][] arr2 = new int[size][2];
		for (int i = 0; i < size; i++) {
			arr2[size - i - 1][0] = arr1[i][0];
			arr2[size - i - 1][1] = arr1[i][1];
		}
		return arr2;
	}

	public tile findStart(char[][] arr1) {
		Integer i, j;
		boolean found = false;
		final tile temp = new tile(0, 0, null);
		for (i = 0; i < arr1.length && !found; i++) {
			for (j = 0; j < arr1[0].length && !found; j++) {
				if (arr1[i][j] == 'S') {
					temp.x = i;
					temp.y = j;
					found = true;
				}
			}
		}
		return temp;
	}

	public boolean isValid(int i, int j) {
		if (i >= 0 && i < map.length && j >= 0 && j < map[i].length && map[i][j] != '#' && map[i][j] != 'S') {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int[][] solveBFS(java.io.File maze) {
		try {
			 Scanner read = new Scanner(maze);
			 int m = read.nextInt();
			 int n = read.nextInt();
			map = new char[m][n];
			for (int i = 0; i < m; i++) {
				 String temp = read.next();
				for (int j = 0; j < n; j++) {
					map[i][j] = temp.charAt(j);
					if (map[i][j] == 'S') {
						noEntry = false;
					}
					if (map[i][j] == 'E') {
						noExit = false;
					}
				}
			}
			read.close();
		} catch ( NumberFormatException e) {
			// TODO Auto-generated catch block
			throw null;
		} catch ( FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw null;
		}

		if (noEntry || noExit) {
			throw null;
		}
		 LinkedBasedQueue S = new LinkedBasedQueue();
		 tile start = findStart(map);
		S.enqueue(start);
		foundEnd = false;
		while (!S.isEmpty()) {
			T = (tile) S.dequeue();
			if (map[T.x][T.y] == 'E') {
				foundEnd = true;
				break;
			}
			if (isValid(T.x + 1, T.y)) {
				 tile next = new tile(T.x + 1, T.y, T);
				if (!(map[T.x + 1][T.y] == 'v')) {
					if (map[T.x + 1][T.y] == '.') {
						map[T.x + 1][T.y] = 'v';
					}
					S.enqueue(next);
				}
			}
			if (isValid(T.x - 1, T.y)) {
				 tile next = new tile(T.x - 1, T.y, T);
				if (!(map[T.x - 1][T.y] == 'v')) {
					if (map[T.x - 1][T.y] == '.') {
						map[T.x - 1][T.y] = 'v';
					}
					S.enqueue(next);
				}
			}
			if (isValid(T.x, T.y + 1)) {
				 tile next = new tile(T.x, T.y + 1, T);
				if (!(map[T.x][T.y + 1] == 'v')) {
					if (map[T.x][T.y + 1] == '.') {
						map[T.x][T.y + 1] = 'v';
					}
					S.enqueue(next);
				}
			}
			if (isValid(T.x, T.y - 1)) {
				 tile next = new tile(T.x, T.y - 1, T);
				if (!(map[T.x][T.y - 1] == 'v')) {
					if (map[T.x][T.y - 1] == '.') {
						map[T.x][T.y - 1] = 'v';
					}
					S.enqueue(next);
				}
			}

		}
		if (foundEnd) {
			path[0][0] = T.x;
			path[0][1] = T.y;
			sizePath = 1;
			while (T.getParent() != null) {
				T = T.getParent();
				path[sizePath][0] = T.x;
				path[sizePath][1] = T.y;
				sizePath++;
			}
			return compress(path, sizePath);
		}

		return null;
	}

	@Override
	public int[][] solveDFS(java.io.File maze) {
		try {
			 Scanner read = new Scanner(maze);
			 int m = read.nextInt();
			 int n = read.nextInt();
			map = new char[m][n];
			for (int i = 0; i < m; i++) {
				 String temp = read.next();
				for (int j = 0; j < n; j++) {
					map[i][j] = temp.charAt(j);
					if (map[i][j] == 'S') {
						noEntry = false;
					}
					if (map[i][j] == 'E') {
						noExit = false;
					}
				}
			}
			read.close();
		} catch (final NumberFormatException e) {
			// TODO Auto-generated catch block
			throw null;
		} catch (final FileNotFoundException e) {
			// TODO Auto-generated catch block
			throw null;
		}
		if (noEntry || noExit) {
			throw null;
		}
		 MyStack S = new MyStack();
		 tile start = findStart(map);
		S.push(start);
		foundEnd = false;
		while (!S.isEmpty()) {
			T = (tile) S.pop();
			if (map[T.x][T.y] == 'E') {
				foundEnd = true;
				break;
			}

			if (isValid(T.x, T.y + 1)) {
				 tile right = new tile(T.x, T.y + 1, T);
				if (map[T.x][T.y + 1] != 'v') {
					if (map[T.x][T.y + 1] == '.') {
						map[T.x][T.y + 1] = 'v';
					}
					S.push(right);
				}
			}
			if (isValid(T.x, T.y - 1)) {
				 tile left = new tile(T.x, T.y - 1, T);
				if (map[T.x][T.y - 1] != 'v') {
					if (map[T.x][T.y - 1] == '.') {
						map[T.x][T.y - 1] = 'v';

					}
					S.push(left);
				}
			}
			if (isValid(T.x + 1, T.y)) {
				 tile down = new tile(T.x + 1, T.y, T);
				if (map[T.x + 1][T.y] != 'v') {
					if (map[T.x + 1][T.y] == '.') {
						map[T.x + 1][T.y] = 'v';
					}
					S.push(down);
				}
			}
			if (isValid(T.x - 1, T.y)) {
				 tile up = new tile(T.x - 1, T.y, T);
				if (map[T.x - 1][T.y] != 'v') {
					if (map[T.x - 1][T.y] == '.') {
						map[T.x - 1][T.y] = 'v';
					}
					S.push(up);
				}
			}

		}
		if (foundEnd) {
			path[0][0] = T.x;
			path[0][1] = T.y;
			sizePath = 1;
			while (T.getParent() != null) {
				T = T.getParent();
				path[sizePath][0] = T.x;
				path[sizePath][1] = T.y;
				sizePath++;
			}
			return compress(path, sizePath);
		}
		return null;
	}

}