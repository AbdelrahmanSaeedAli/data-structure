package eg.edu.alexu.csd.datastructure.maze.cs39;

public class tile {
	int x;
	int y;
	tile parent;
	boolean visited = false;

	public tile(final int i,final int j,final tile parent) {
		x = i;
		y = j;
		this.parent = parent;
	}

	public tile getParent() {
		return parent;
	}
}
