package eg.edu.alexu.csd.datastructure.linkedList.cs39_46;

import static org.junit.Assert.*;

import org.junit.Test;

public class testing {

	@Test
	public void test() {
		Single list = new Single();
		assertTrue(list.isEmpty());
		list.add(500);
		assertFalse(list.isEmpty());
		list.add(1, 200);
		assertEquals(200, list.get(1));
		list.set(1, 300);
		assertEquals(300, list.get(1));
		assertEquals(2, list.size());
		list.add(200);
		assertEquals(200, list.get(2));
		assertEquals(3, list.size());
		list.add(2, 22);
		assertEquals(200, list.get(3));
		list.remove(2);
		assertEquals(300, list.get(1));
		assertEquals(3, list.size());
		list.clear();
		list.add(300);
		list.add(100);
		list.add(200);
		assertTrue(list.contains(200));
		assertFalse(list.contains(500));

	}

	@Test
	public void test2() {
		Double list = new Double();
		assertTrue(list.isEmpty());
		list.add(500);
		list.add(200);
		assertEquals(500, list.get(0));
		assertEquals(200, list.get(1));
		list.add(2, 300);
		assertEquals(300, list.get(2));
		list.clear();
		assertTrue(list.isEmpty());
		list.add(500);
		list.add(200);
		list.set(1, 100);
		assertFalse(list.isEmpty());
		assertEquals(100, list.get(1));
		assertEquals(2, list.size());
		assertTrue(list.contains(500));
		assertFalse(list.contains(600));
		list.remove(0);
		assertFalse(list.contains(500));
		assertEquals(100, list.get(0));
	}
	@Test
	public void test3() {
		Single list =new Single();
		list.add(0);
		list.add(1);
		list.add(2);
		list.add(3);
		assertEquals(4, list.size());
		Single sub = (Single) list.sublist(1,2);
		assertEquals(2, sub.size());
		assertEquals(1, sub.get(0));
		assertEquals(2, sub.get(1));
		
	}

}