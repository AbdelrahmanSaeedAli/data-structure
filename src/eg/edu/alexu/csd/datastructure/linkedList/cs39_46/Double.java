package eg.edu.alexu.csd.datastructure.linkedList.cs39_46;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class Double implements ILinkedList {
	dnode head = new dnode();
	int size;

	public Double() {
		head = null;
		size = 0;
	}

	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub
		if (index > 0) {
			dnode current = head;
			final dnode temp = new dnode();
			temp.data = element;
			for (int i = 1; i < index; i++) {
				if (current.next == null) {
					return;
				}
				current = current.next;
			}
			temp.previous = current;
			current.next = temp;
			current = current.next;
			temp.next = current;
			current.previous = temp;
			size++;
		}
		if (index == 0) {
			final dnode temp = new dnode();
			temp.next = head;
			head.previous = temp;
			head = temp;
			head.previous = null;
			size++;
		}
	}

	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub
		dnode current = new dnode();
		final dnode temp = new dnode();
		temp.data = element;
		if (head == null) {
			head = temp;
		} else {
			current = head;
			while (current.next != null) {
				current = current.next;
			}
			current.next = temp;
			temp.next = null;
			temp.previous = current;
			size++;
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		head = null;
		size = 0;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		dnode current = new dnode();
		current = head;
		while (current.next != null) {
			if (o.equals(current.data)) {
				return true;
			}
			current = current.next;
		}
		return false;
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			dnode current = new dnode();
			current = head;
			for (int i = 0; i < index; i++) {
				if (current.next == null) {
					throw null;
				}
				current = current.next;
			}

			return current.data;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void remove(int index) {
		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			if (index == 0) {
				head = head.next;
			} else {
				dnode current = new dnode();
				current = head;
				for (int i = 1; i < index; i++) {
					if (current.next == null) {
						throw null;
					}
					current = current.next;
				}
				current.next = current.next.next;
				current.next.next.previous = current;
			}
			size--;
		}
	}

	@Override
	public void set(int index, Object element) {

		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			dnode current = new dnode();
			final dnode temp = new dnode();
			current = head;
			for (int i = 1; i < index; i++) {
				if (current.next == null) {
					throw null;
				}
				current = current.next;
			}
			temp.next = current.next.next;
			temp.previous = current.next;
			current.next = temp;
			temp.data = element;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		int count = 1;
		dnode current = new dnode();
		current = head;
		while (current.next != null) {
			count++;
			current = current.next;
		}
		return count;
	}

	@Override
	public ILinkedList sublist(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		if (fromIndex >= 0 && fromIndex < size() && toIndex >= fromIndex && toIndex <= size()) {
			final Double sublist = new Double();
			for (int i = fromIndex; i <= toIndex; i++) {
				sublist.add(get(i));
				;
			}
			return sublist;
		} else {
			throw null;
		}
	}
}