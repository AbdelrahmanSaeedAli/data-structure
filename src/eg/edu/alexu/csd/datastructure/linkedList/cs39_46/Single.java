package eg.edu.alexu.csd.datastructure.linkedList.cs39_46;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class Single implements ILinkedList {
	node head = new node();
	int size;

	public Single() {
		head = null;
		size = 0;
	}

	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub
		if (index > 0) {
			node current = head;
			final node temp = new node();
			temp.data = element;
			for (int i = 1; i < index; i++) {
				if (current.next == null) {
					return;
				}
				current = current.next;
			}
			temp.next = current.next;
			current.next = temp;
			size++;
		}
		if (index == 0) {
			final node temp = new node();
			temp.next = head;
			head = temp;
			size++;
		}
	}

	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub
		node current = new node();
		final node temp = new node();
		temp.data = element;
		if (head == null) {
			head = temp;
		} else {
			current = head;
			while (current.next != null) {
				current = current.next;
			}
			current.next = temp;
			temp.next = null;
			size++;
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		head = null;
		size = 0;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub

		node current = new node();
		current = head;
		while (current != null) {
			if (o.equals(current.data)) {
				return true;
			}
			current = current.next;
		}
		return false;

	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			node current = new node();
			current = head;
			for (int i = 0; i < index; i++) {
				if (current.next == null) {
					throw null;
				}
				current = current.next;
			}

			return current.data;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void remove(int index) {
		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			if (index == 0) {
				head = head.next;
			} else {
				node current = new node();
				current = head;
				for (int i = 1; i < index; i++) {
					if (current.next == null) {
						throw null;
					}
					current = current.next;
				}
				current.next = current.next.next;
			}
			size--;
		}
	}

	@Override
	public void set(int index, Object element) {

		// TODO Auto-generated method stub
		if (index < 0) {
			throw null;
		} else {
			node current = new node();
			final node temp = new node();
			current = head;
			for (int i = 1; i < index; i++) {
				if (current.next == null) {
					throw null;
				}
				current = current.next;
			}
			temp.next = current.next.next;
			current.next = temp;
			temp.data = element;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		int count = 1;
		node current = new node();
		current = head;
		while (current.next != null) {
			count++;
			current = current.next;
		}
		return count;
	}

	@Override
	public ILinkedList sublist(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		if (fromIndex >= 0 && fromIndex < size() && toIndex >= fromIndex && toIndex <= size()) {
			final Single sublist = new Single();
			for (int i = fromIndex; i <= toIndex; i++) {
				sublist.add(get(i));
				;
			}
			return sublist;
		} else {
			throw null;
		}
	}
}