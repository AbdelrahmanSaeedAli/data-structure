package eg.edu.alexu.csd.datastructure.linkedList.cs39_46;

import eg.edu.alexu.csd.datastructure.linkedList.IPolynomialSolver;

public class PolynomialClass implements IPolynomialSolver {
	Single po1 = new Single();
	Single po2 = new Single();
	Single po3 = new Single();
	Single po4 = new Single();

	@Override
	public int[][] add(char poly1, char poly2) {

		// TODO Auto-generated method stub
		po4.clear();
		node current1 = null;
		Single list1 = new Single();
		node current2 = null;
		Single list2 = new Single();
		if (poly1 >= 'A' && poly1 <= 'C' && poly2 >= 'A' && poly2 <= 'C' && poly1 != poly2) {

			if (poly1 == 'A') {
				list1 = po1;
				current1 = po1.head;
			}
			if (poly1 == 'B') {
				list1 = po3;
				current1 = po2.head;
			}
			if (poly1 == 'C') {
				list1 = po3;
				current1 = po3.head;
			}
			if (poly2 == 'A') {
				list2 = po1;
				current2 = po1.head;
			}
			if (poly2 == 'B') {
				list2 = po3;
				current2 = po2.head;
			}
			if (poly2 == 'C') {
				list2 = po3;
				current2 = po3.head;
			}

			final node result = po4.head;
			if (list1.isEmpty()) {
				return null;
			}
			if (list2.isEmpty()) {
				return null;
			}

			if (list1.size() >= list2.size()) {
				while (current2 != null) {
					final element one = (element) current1.data;
					final element two = (element) current2.data;
					final element r = (element) result.data;
					if (one.exp > two.exp) {
						r.coeff = one.coeff;
						r.exp = one.exp;
						current1 = current1.next;
					} else if (two.exp > one.exp) {
						r.coeff = two.coeff;
						r.exp = two.exp;
						current2 = current2.next;
					} else if (two.exp == one.exp) {
						r.coeff = one.coeff + two.coeff;
						r.exp = one.exp;
						current1 = current1.next;
						current2 = current2.next;
					}
					po4.add(r);
				}
				while (current1 != null) {
					po4.add(current1);
					current1 = current1.next;
				}
			}
			if (list2.size() > list1.size) {
				while (current1 != null) {
					final element one = (element) current1.data;
					final element two = (element) current2.data;
					final element r = (element) result.data;
					if (one.exp > two.exp) {
						r.coeff = one.coeff;
						r.exp = one.exp;
						current1 = current1.next;
					} else if (two.exp > one.exp) {
						r.coeff = two.coeff;
						r.exp = two.exp;
						current2 = current2.next;
					} else if (two.exp == one.exp) {
						r.coeff = one.coeff + two.coeff;
						r.exp = one.exp;
						current1 = current1.next;
						current2 = current2.next;
					}
					po4.add(r);
				}
				while (current2 != null) {
					po4.add(current2);
					current1 = current2.next;
				}
			}

			final int[][] A = new int[po4.size()][2];
			for (int j = 0; j < po4.size(); j++) {
				final element n = (element) po4.get(j);
				A[j][0] = n.coeff;
				A[j][1] = n.exp;
			}
			return A;
		} else

		{
			throw null;
		}
	}

	@Override
	public void clearPolynomial(char poly) {
		// TODO Auto-generated method stub
		if (poly == 'A') {
			po1.clear();
		}
		if (poly == 'B') {
			po1.clear();
		}
		if (poly == 'c') {
			po1.clear();
		}
	}

	@Override
	public float evaluatePolynomial(char poly, float value) {
		// TODO Auto-generated method stub

		if (poly >= 'A' && poly <= 'C') {
			float val = 0;
			node current;
			Single list = new Single();
			if (poly == 'A') {
				list = po1;
				current = po1.head;
			}
			if (poly == 'B') {
				list = po2;
				current = po2.head;
			}
			if (poly == 'C') {
				list = po3;
				current = po3.head;
			}

			if (list.isEmpty()) {
				throw null;
			}
			current = list.head;
			while (current != null) {
				final element one = (element) current.data;
				val += one.coeff * Math.pow(value, one.exp);
				current = current.next;
			}
			return val;
		} else {
			throw null;
		}
	}

	public boolean isArranged(int[][] terms) {
		final int max = terms[0][1];
		for (int i = 1; i < terms.length; i++) {
			if (terms[i][1] > max) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int[][] multiply(char poly1, char poly2) {
		po4.clear();
		node current1 = null;
		Single list1 = new Single();
		node current2 = null;
		Single list2 = new Single();
		if (poly1 >= 'A' && poly1 <= 'C' && poly2 >= 'A' && poly2 <= 'C' && poly1 != poly2) {
			if (poly1 == 'A') {
				list1 = po1;
				current1 = po1.head;
			}
			if (poly1 == 'B') {
				list1 = po3;
				current1 = po2.head;
			}
			if (poly1 == 'C') {
				list1 = po3;
				current1 = po3.head;
			}
			if (poly2 == 'A') {
				list2 = po1;
				current2 = po1.head;
			}
			if (poly2 == 'B') {
				list2 = po3;
				current2 = po2.head;
			}
			if (poly2 == 'C') {
				list2 = po3;
				current2 = po3.head;
			}

			final node result = po4.head;
			for (int i = 0; i < list1.size(); i++) {
				final element one = (element) current1.data;
				for (int j = 0; j < list2.size(); j++) {
					final element two = (element) current2.data;
					final element r = (element) result.data;
					r.coeff = one.coeff * two.coeff;
					r.exp = one.exp + two.exp;
					po4.add(r);
					current2 = current2.next;
				}
				current1 = current1.next;
			}

			for (int i = 0; i < po4.size(); i++) {
				node currentsort = po4.head;
				for (int j = 0; j < po4.size() - 1; j++) {
					final element one = (element) currentsort.data;
					final element two = (element) currentsort.next.data;
					if (one.exp < two.exp) {
						final element temp = new element();
						temp.coeff = one.coeff;
						temp.exp = one.exp;
						one.coeff = two.coeff;
						one.exp = two.exp;
						two.exp = temp.exp;
						two.coeff = temp.coeff;
					}
				}
				currentsort = currentsort.next;
			}
			final int[][] A = new int[po4.size()][2];
			for (int j = 0; j < po4.size(); j++) {
				final element n = (element) po4.get(j);
				A[j][0] = n.coeff;
				A[j][1] = n.exp;
			}

			return A;
		} else {
			throw null;
		}

	}

	@Override
	public String print(char poly) {
		// TODO Auto-generated method stub

		if (poly >= 'A' && poly <= 'C' || poly == 'R') {
			node current;
			Single list = new Single();
			if (poly == 'A') {
				list = po1;
				current = po1.head;
			}
			if (poly == 'B') {
				list = po3;
				current = po2.head;
			}
			if (poly == 'C') {
				list = po3;
				current = po3.head;
			}
			if (poly == 'R') {
				list = po4;
				current = po4.head;
			}

			String a = new String("");
			current = list.head;
			while (current != null) {
				final element one = (element) current.data;
				if (one.coeff > 0) {
					a = a + '+' + String.valueOf(one.coeff) + 'x' + '^' + String.valueOf(one.exp);
				} else {
					a = a + String.valueOf(one.coeff) + 'x' + '^' + String.valueOf(one.exp);
				}
				current = current.next;
			}
			return a;
		} else {
			throw null;
		}
	}

	@Override
	public void setPolynomial(char poly, int[][] terms) {
		// TODO Auto-generated method stub
		node current;
		Single list = new Single();
		if (poly >= 'A' && poly <= 'C' && isArranged(terms)) {
			if (poly == 'A') {
				list = po1;
				current = po1.head;
			}
			if (poly == 'B') {
				list = po3;
				current = po2.head;
			}
			if (poly == 'C') {
				list = po3;
				current = po3.head;
			}
			for (int i = 0; i < terms.length; i++) {
				final element term = new element();
				term.coeff = terms[0][i];
				term.exp = terms[0][i];
				list.add(term);
			}
			current = list.head;
			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					final element one = (element) current.data;
					final element two = (element) list.get(j);
					if (one.exp == two.exp) {
						final element result = new element();
						result.coeff = one.coeff + two.coeff;
						list.remove(j);
						list.set(i, result);
					}
				}
				current = current.next;
			}

			for (int i = 0; i < list.size(); i++) {
				current = list.head;
				for (int j = 0; j < list.size() - 1; j++) {
					final element one = (element) current.data;
					final element two = (element) current.next.data;
					if (one.exp < two.exp) {
						final element temp = new element();
						temp.coeff = one.coeff;
						temp.exp = one.exp;
						one.coeff = two.coeff;
						one.exp = two.exp;
						two.exp = temp.exp;
						two.coeff = temp.coeff;
					}
				}
				current = current.next;
			}

		} else {
			throw null;
		}
	}

	@Override
	public int[][] subtract(char poly1, char poly2) {
		// TODO Auto-generated method stub
		po4.clear();
		if (poly1 >= 'A' && poly1 <= 'C' && poly2 >= 'A' && poly2 <= 'C' && poly1 != poly2) {
			node current1 = null;
			Single list1 = new Single();
			node current2 = null;
			Single list2 = new Single();
			if (poly1 == 'A') {
				list1 = po1;
				current1 = list1.head;
			}
			if (poly1 == 'B') {
				list1 = po2;
				current1 = list1.head;
			}
			if (poly1 == 'C') {
				list1 = po3;
				current1 = list1.head;
			}
			if (poly2 == 'A') {
				list2 = po1;
				current2 = list2.head;
			}
			if (poly2 == 'B') {
				list2 = po3;
				current2 = list2.head;
			}
			if (poly2 == 'C') {
				list2 = po3;
				current2 = list2.head;
			}

			final node result = po4.head;
			if (list1.isEmpty()) {
				return null;
			}
			if (list2.isEmpty()) {
				return null;
			} else {
				if (list1.size() >= list2.size()) {
					while (current2 != null) {
						final element one = (element) current1.data;
						final element two = (element) current2.data;
						final element r = (element) result.data;
						if (one.exp > two.exp) {
							r.coeff = one.coeff;
							r.exp = one.exp;
							current1 = current1.next;
						} else if (two.exp > one.exp) {
							r.coeff = two.coeff;
							r.exp = two.exp;
							current2 = current2.next;
						} else if (two.exp == one.exp) {
							r.coeff = one.coeff + two.coeff;
							r.exp = one.exp;
							current1 = current1.next;
							current2 = current2.next;
						}
						if (r.coeff != 0) {
							po4.add(r);
						}
					}
					while (current1 != null) {
						po4.add(current1);
						current1 = current1.next;
					}
				}
				if (list2.size() > list1.size) {
					while (current1 != null) {
						final element one = (element) current1.data;
						final element two = (element) current2.data;
						final element r = (element) result.data;
						if (one.exp > two.exp) {
							r.coeff = one.coeff;
							r.exp = one.exp;
							current1 = current1.next;
						} else if (two.exp > one.exp) {
							r.coeff = two.coeff;
							r.exp = two.exp;
							current2 = current2.next;
						} else if (two.exp == one.exp) {
							r.coeff = one.coeff - two.coeff;
							r.exp = one.exp;
							current1 = current1.next;
							current2 = current2.next;
						}
						po4.add(r);
					}
					while (current2 != null) {
						po4.add(current2);
						current1 = current2.next;
					}
				}
			}
			final int[][] A = new int[po4.size()][2];
			for (int j = 0; j < po4.size(); j++) {
				final element n = (element) po4.get(j);
				A[j][0] = n.coeff;
				A[j][1] = n.exp;
			}
			return A;

		} else {
			throw null;
		}
	}
}