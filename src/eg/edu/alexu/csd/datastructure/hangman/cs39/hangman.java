package eg.edu.alexu.csd.datastructure.hangman.cs39;

import java.util.Random;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class hangman implements IHangman {
	private String[] words;
	private String secretWord;
	private String dashes = new String("");
	private int max;
	private int wrongGuesses = 0;
	private int found;
	private int j = 0;

	private final char[] usedChar = new char[15];

	@Override
	public String guess(Character c) {

		new Character(c);
		final Character a = Character.toLowerCase(c);
		new Character(c);
		final Character b = Character.toUpperCase(c);

		if (wrongGuesses == max) {
			return null;
		}
		if (c == null) {
			return dashes;
		}
		if (dashes.equals(secretWord)) {
			return null;
		} else {
			found = 0;
			for (int i = 0; i < secretWord.length(); i++) {

				if (a.charValue() == secretWord.charAt(i) || b.charValue() == secretWord.charAt(i)) {
					found = 1;
					dashes = dashes.substring(0, i) + secretWord.charAt(i) + dashes.substring(i + 1);

				}
			}

			if (found == 0) {

				wrongGuesses++;
				usedChar[j] = c.charValue();
				j++;
			}

		}
		if (wrongGuesses == max) {
			return null;
		} else {
			return dashes;
		}
	}

	@Override
	public String selectRandomSecretWord() {
		final Random random = new Random();
		int index;
		index = random.nextInt(words.length);
		secretWord = words[index];
		setDashes();
		return secretWord;
	}

	// public String[] readfromfile(String fileName){
	// String[] array= new String[50];
	// y= new Scanner(new File(fileName));
	// while(y.hasNext()){
	// array[i]=y.next();
	// i++;}
	// return array;}

	public void setDashes() {
		final int n = secretWord.length();
		for (int i = 0; i < n; i++) {
			dashes = new String(dashes.concat("-"));
		}
	}

	@Override
	public void setDictionary(String[] words) {
		this.words = words;
	}

	@Override
	public void setMaxWrongGuesses(Integer max) {
		if (max == null) {
			this.max = 0;
		}
		this.max = max.intValue();
	}

	public void setSecretWord(String secretWord) {
		this.secretWord = secretWord;
	}

}
