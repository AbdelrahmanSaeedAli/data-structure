package eg.edu.alexu.csd.datastructure.hangman.cs39;
import java.util.Scanner;
public class GameTest {

	public static void main(String[] args) {
		hangman x=new hangman();
		
//		String[] a=x.readfromfile(new String("input.txt"));
		String[] a = new String[]{"random","words","abdelrahman","khaled"};
		x.setDictionary(a);
		x.selectRandomSecretWord();

		x.setMaxWrongGuesses(new Integer(5));
		
		String word= new String ("Barie");
		
		Scanner read = new Scanner(System.in);
		while(word != null){
		char k = read.next().charAt(0);
		Character c = new Character(k);
		word=x.guess(c);
		System.out.println(word);
		}
	}

}
