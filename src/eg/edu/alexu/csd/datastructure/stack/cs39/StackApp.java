package eg.edu.alexu.csd.datastructure.stack.cs39;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

/**
 * * Takes a symbolic/numeric infix expression as input and converts it to *
 * postfix notation. There is no assumption on spaces between terms or the *
 * length of the term (e.g., two digits symbolic or numeric term)
 *
 * @param expression
 *            infix expression *
 * @return postfix expression
 */
public class StackApp implements IExpressionEvaluator {
	/**
	 * * Evaluate a postfix numeric expression, with a single space separator
	 *
	 * @param expression
	 *            postfix expression
	 * @return the expression evaluated value
	 */
	@Override
	public int evaluate(String expression) {
		if (expression == null || expression.length() == 0 || isOperator(expression.charAt(0))) {
			throw null;
		}

		final MyStack Stack = new MyStack();
		int counter;
		String temp2 = new String("");
		for (int i = 0; i < expression.length(); i++) {
			counter = 0;
			temp2 = "";
			if (isOperand(expression.charAt(i))) {
				int temp = i + 1;
				if (!(temp < expression.length())) {
					Stack.push(expression.charAt(i));
				} else {
					temp2 = temp2 + expression.charAt(i);
					while (isOperand(expression.charAt(temp))) {
						counter++;
						temp++;
						temp2 = temp2 + expression.charAt(temp);
					}
					i = i + counter;
					Stack.push(temp2);
				}
			} else if (isOperator(expression.charAt(i))) {
				if (Stack.size() < 2) {
					throw null;
				} else {
					final String x = Stack.pop().toString();
					final String y = Stack.pop().toString();
					Stack.push(Solve(y, x, expression.charAt(i)));
				}
			}
		}
		if (Stack.size() == 1) {
			return Integer.valueOf(Stack.pop().toString());
		} else if (Stack.size() > 1) {
			return 0;
		} else {
			throw null;
		}
	}

	@Override
	public String infixToPostfix(String expression) {
		if (expression == null || expression.length() == 0 || isOperator(expression.charAt(0))
				|| isOperator(expression.charAt(expression.length() - 1)) || expression.charAt(0) == ')'
				|| expression.charAt(expression.length() - 1) == '(') {
			throw null;
		}
		final MyStack Stack = new MyStack();
		String post = new String("");
		int count1 = 0, count2 = 0;
		expression = expression.replaceAll("\\s*", "");
		for (int i = 0; i < expression.length(); i++) {
			if (expression.charAt(i) == ')') {
				count1++;
				if (Stack.isEmpty()) {
					throw null;
				} else {
					try {
						while ((Character) Stack.peek() != '(') {
							post = post + " " + Stack.pop().toString();
						}
					} catch (final Exception e) {
						throw new RuntimeException("catch");
					}
					if (Stack.isEmpty()) {
						throw null;
					}
					Stack.pop();
				}
			} else if (expression.charAt(i) == '(') {
				count2++;
				Stack.push(expression.charAt(i));
			} else if (isOperator(expression.charAt(i))) {
				if (isOperator(expression.charAt(i + 1))
						|| expression.charAt(i + 1) == ' ' && isOperator(expression.charAt(i + 2))) {
					throw null;
				} else {
					if (Stack.isEmpty()) {
						Stack.push(expression.charAt(i));
					} else if (expression.charAt(i) == '+' || expression.charAt(i) == '-') {
						while (!Stack.isEmpty() && (Character) Stack.peek() != '(') {
							post = post + " " + Stack.pop().toString();
						}
						Stack.push(expression.charAt(i));
					} else {
						if (isPreced(expression.charAt(i), Stack.peek().toString())) {
							Stack.push(expression.charAt(i));
						} else {
							post = post + " " + Stack.pop().toString();
							Stack.push(expression.charAt(i));
						}
					}
				}
			} else {
				if (post.length() == 0) {
					post = post + expression.charAt(i);
				} else {
					post = post + " " + expression.charAt(i);
				}
			}
		}
		if (count1 != count2) {
			throw null;
		}
		while (!Stack.isEmpty()) {
			if ((Character) Stack.peek() == '(') {
				throw null;
			} else {
				post = post + " " + Stack.pop().toString();
			}
		}
		return post;
	}

	/**
	 * * checks wether the character is an operand or not
	 */
	public boolean isOperand(char x) {
		if (x >= '0' && x <= '9') {
			return true;
		} else if (x >= 'a' && x <= 'z') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * * checks wether the character is an operator or not
	 */
	public boolean isOperator(char x) {
		if (x == '+' || x == '-' || x == '/' || x == '*') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * *checks the precedence of y and x
	 *
	 * @param x
	 *            element in the stack
	 * @param y
	 *            element in the expression
	 */
	public boolean isPreced(char x, String y) {
		if ((x == '*' || x == '/') && (y.charAt(0) == '+' || y.charAt(0) == '-' || y.charAt(0) == '(')) {
			return true;
		}
		if ((x == '+' || x == '-') && y.charAt(0) == '(') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * *preforms a mathematical operation
	 *
	 * @param x
	 *            operand
	 * @param y
	 *            operand
	 * @param z
	 *            operator
	 * @return result of the mathematical operations
	 */
	public String Solve(String x, String y, char z) {
		Integer result = new Integer(0);
		if (z == '+') {
			result = Integer.valueOf(x) + Integer.valueOf(y);
		} else if (z == '-') {
			result = Integer.valueOf(x) - Integer.valueOf(y);
		} else if (z == '*') {
			result = Integer.valueOf(x) * Integer.valueOf(y);
		} else if (z == '/') {
			result = Integer.valueOf(x) / Integer.valueOf(y);
		}
		return result.toString();
	}
}
