package eg.edu.alexu.csd.datastructure.stack.cs39;

import eg.edu.alexu.csd.datastructure.stack.IStack;

public class MyStack implements IStack {
	private final int maxSize = 100002;
	private final Object[] arr = new Object[maxSize];
	private int top = -1;

	/*
	 * (non-Javadoc) adds element to a certain index
	 */
	@Override
	public void add(int index, Object element) {
		if (index < 0 || index > top + 1) {
			throw null;
		} else {
			int temp = top + 1;
			for (int i = index; i < top + 1; i++) {
				arr[temp] = arr[temp - 1];
				temp--;
			}
			arr[index] = element;
			top++;
		}
	}

	/*
	 * (non-Javadoc) Check wether the stack is empty or not
	 */
	@Override
	public boolean isEmpty() {
		if (top == -1) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc) returns the top element of the stack
	 */
	@Override
	public Object peek() {
		if (isEmpty()) {
			throw null;
		} else {
			return arr[top];
		}
	}

	/*
	 * (non-Javadoc) removes last element in the stack and returns it
	 */
	@Override
	public Object pop() {
		if (isEmpty()) {
			throw null;
		} else {
			top--;
			final Object x = arr[top + 1];
			arr[top + 1] = null;
			return x;
		}
	}

	/*
	 * (non-Javadoc) add an element to the top of the stack
	 */
	@Override
	public void push(Object element) {
		top++;
		arr[top] = element;
	}

	/*
	 * (non-Javadoc) returns the size of the stack
	 */
	@Override
	public int size() {
		return top + 1;
	}
}