package eg.edu.alexu.csd.datastructure.iceHockey.cs39;

import java.awt.Point;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

public class PlayersFinder implements IPlayersFinder {

	private final Point[] players = new Point[100];
	boolean[][] checked;
	int minX, minY, maxX, maxY;
	int counter;
	int number = 0;

	public void calculate(int threshold, int counter) {
		int squares = threshold / 4;
		if (threshold % 4 != 0) {
			squares++;
		}
		if (counter >= squares) {
			players[number] = new Point();
			players[number].x = maxX + 1 + minX;
			players[number].y = maxY + 1 + minY;
			number++;
		}
	}

	@Override
	public Point[] findPlayers(String[] photo, int team, int threshold) {
		if (photo.length == 0) {
			return null;
		}
		checked = new boolean[photo.length][photo[0].length()];

		for (int i = 0; i < photo.length; i++) {
			for (int j = 0; j < photo[0].length(); j++) {
				if (Integer.toString(team).charAt(0) == photo[i].charAt(j) && !checked[i][j]) {
					checked[i][j] = true;
					counter = 1;
					minX = j;
					minY = i;
					maxX = j;
					maxY = i;
					recursion(photo, team, i, j);
					calculate(threshold, counter);

				}
			}
			sortPlayers();
		}
		if (number == 0) {
			return null;
		}

		int k = 0;
		final Point[] center = new Point[number];
		while (k < number) {
			center[k] = players[k];
			k++;
		}

		return center;
	}

	public void recursion(String[] photo, int team, int i, int j) {

		for (int m = i - 1; m <= i + 1; m++) {
			for (int n = j - 1; n <= j + 1; n++) {

				if (m >= 0 && n >= 0 && n < photo[0].length() && m < photo.length && (m == i - 1 && n == j
						|| m == i && (n == j - 1 || n == j + 1) || m == i + 1 && n == j)) {

					if (!checked[m][n] && photo[m].charAt(n) == Integer.toString(team).charAt(0)) {

						checked[m][n] = true;

						if (n < minX) {
							minX = n;
						}
						if (m < minY) {
							minY = m;
						}
						if (n > maxX) {
							maxX = n;
						}
						if (m > maxY) {
							maxY = m;
						}
						counter++;
						recursion(photo, team, m, n);
					}
				}
			}
		}
	}

	public void sortPlayers() {
		Point none;
		for (int i = 0; i < number; i++) {
			for (int j = i + 1; j < number; j++) {
				if (players[i].x == players[j].x && players[i].y > players[j].y || players[i].x > players[j].x) {
					none = players[i];
					players[i] = players[j];
					players[j] = none;
				}
			}
		}
	}

}
